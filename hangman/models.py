from nturl2path import url2pathname
from django.db import models
import random

# Create your models here.

class GameModel(models.Model):
    def choose():
        word_list = [
        "HELLO",
        "GOALS",
        "NEVER",
        "APPLE",
        "MAPLE",
        "CLUES",
        "BOAST",
        "CLOUT",
        ]

        return random.choice(word_list)

    start = models.BooleanField(default=False)
    word = models.CharField(max_length=5, default=choose)
    letter1_bool = models.BooleanField(default=False)
    letter2_bool = models.BooleanField(default=False)
    letter3_bool = models.BooleanField(default=False)
    letter4_bool = models.BooleanField(default=False)
    letter5_bool = models.BooleanField(default=False)
    letter1 = models.CharField(max_length=1, default="_")
    letter2 = models.CharField(max_length=1, default="_")
    letter3 = models.CharField(max_length=1, default="_")
    letter4 = models.CharField(max_length=1, default="_")
    letter5 = models.CharField(max_length=1, default="_")
    guess1_bool = models.BooleanField(default=False)
    guess2_bool = models.BooleanField(default=False)
    guess3_bool = models.BooleanField(default=False)
    guess4_bool = models.BooleanField(default=False)
    guess5_bool = models.BooleanField(default=False)
    guess6_bool = models.BooleanField(default=False)
    guess7_bool = models.BooleanField(default=False)
    guess8_bool = models.BooleanField(default=False)
    guess9_bool = models.BooleanField(default=False)
    guess10_bool = models.BooleanField(default=False)
    guess11_bool = models.BooleanField(default=False)
    guess12_bool = models.BooleanField(default=False)
    guess13_bool = models.BooleanField(default=False)
    guess14_bool = models.BooleanField(default=False)
    guess15_bool = models.BooleanField(default=False)
    guess16_bool = models.BooleanField(default=False)
    guess1 = models.CharField(max_length=1, default=None, null=True)
    guess2 = models.CharField(max_length=1, default=None, null=True)
    guess3 = models.CharField(max_length=1, default=None, null=True)
    guess4 = models.CharField(max_length=1, default=None, null=True)
    guess5 = models.CharField(max_length=1, default=None, null=True)
    guess6 = models.CharField(max_length=1, default=None, null=True)
    guess7 = models.CharField(max_length=1, default=None, null=True)
    guess8 = models.CharField(max_length=1, default=None, null=True)
    guess9 = models.CharField(max_length=1, default=None, null=True)
    guess10 = models.CharField(max_length=1, default=None, null=True)
    guess11 = models.CharField(max_length=1, default=None, null=True)
    guess12 = models.CharField(max_length=1, default=None, null=True)
    guess13 = models.CharField(max_length=1, default=None, null=True)
    guess14 = models.CharField(max_length=1, default=None, null=True)
    guess15 = models.CharField(max_length=1, default=None, null=True)
    guess16 = models.CharField(max_length=1, default=None, null=True)
    stage = models.SmallIntegerField(default=1)
    num_guesses = models.SmallIntegerField(default=0)

    