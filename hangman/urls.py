from django.urls import path

from hangman.views import (
    StartGame,
    PlayHangman,
    guess_letter,
    you_lose,
    you_win,
)

urlpatterns = [
    path("game/", StartGame.as_view(), name="play_game"),
    path("game/<int:pk>/", PlayHangman.as_view(), name="play_hangman"),
    path("game/<int:pk>/guess", guess_letter, name="make_guess"),
    path("game/lose/", you_lose, name="loser"),
    path("game/win/", you_win, name="winner"),
]