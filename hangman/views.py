from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse_lazy
import random
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

from hangman.models import GameModel

# Create your views here.

class StartGame(CreateView):
    model = GameModel
    template_name = "hangman/game.html"
    fields = []

    def get_success_url(self):
        return reverse_lazy("play_hangman", kwargs={"pk": self.object.pk})



class PlayHangman(DetailView):
    model = GameModel
    template_name = "hangman/gameplay.html"
    context_object_name = "hangman"



def guess_letter(request, pk):
    if request.method == "POST" and pk:
        guess = request.POST["guess1"]
        guess = guess.upper()
        instance = GameModel.objects.get(pk=pk)
        word1 = getattr(instance, "word")
        num_g = getattr(instance, "num_guesses")
        letter1 = getattr(instance, "letter1")
        letter2 = getattr(instance, "letter2")
        letter3 = getattr(instance, "letter3")
        letter4 = getattr(instance, "letter4")
        letter5 = getattr(instance, "letter5")
        if num_g == 0:
            GameModel.objects.update(guess1=guess)
        elif num_g == 1:
            GameModel.objects.update(guess2=guess)
        elif num_g == 2:
            GameModel.objects.update(guess3=guess)
        elif num_g == 3:
            GameModel.objects.update(guess4=guess)
        elif num_g == 4:
            GameModel.objects.update(guess5=guess)
        elif num_g == 5:
            GameModel.objects.update(guess6=guess)
        elif num_g == 6:
            GameModel.objects.update(guess7=guess)
        elif num_g == 7:
            GameModel.objects.update(guess8=guess)
        elif num_g == 8:
            GameModel.objects.update(guess9=guess)
        elif num_g == 9:
            GameModel.objects.update(guess10=guess)
        elif num_g == 10:
            GameModel.objects.update(guess11=guess)
        elif num_g == 11:
            GameModel.objects.update(guess12=guess)
        elif num_g == 12:
            GameModel.objects.update(guess13=guess)
        elif num_g == 13:
            GameModel.objects.update(guess14=guess)
        elif num_g == 14:
            GameModel.objects.update(guess15=guess)
        elif num_g == 15:
            GameModel.objects.update(guess16=guess)
            
        correct_guess = False
        GameModel.objects.update(num_guesses=(num_g + 1))
        if letter1 != "_" and letter2 != "_" and letter3 != "_" and letter4 != "_" and letter5 != "_":
            return redirect("winner")
        if word1[0] == guess:
            GameModel.objects.update(letter1=guess)
            correct_guess = True
        if word1[1] == guess:
            GameModel.objects.update(letter2=guess)
            correct_guess = True
        if word1[2] == guess:
            GameModel.objects.update(letter3=guess)
            correct_guess = True
        if word1[3] == guess:
            GameModel.objects.update(letter4=guess)
            correct_guess = True
        if word1[4] == guess:
            GameModel.objects.update(letter5=guess)
            correct_guess = True
        if not correct_guess:
            stage = getattr(instance, "stage")
            stage += 1
            GameModel.objects.update(stage=stage)
            if stage == 12:
                return redirect("loser")
        
        return redirect("play_hangman", pk=pk)
    else:
        return redirect("play_hangman", pk=pk)

    
def you_win(request):
    return render(request, "hangman/winner.html")

def you_lose(request):
    return render(request, "hangman/loser.html")

    


# def play_hangman(request):
#     if request.method == "POST":
#         form = request.POST
#         if not guess1:
#             guess1 = request.POST.get("guess")
#         elif not guess2:
#             guess2 = request.POST.get("guess")
#         elif not guess3:
#             guess3 = request.POST.get("guess")
#         elif not guess4:
#             guess4 = request.POST.get("guess")
#     else:
#         form = ""
#         guess1 = ""
#         guess2 = ""
#         guess3 = ""
#         guess4 = ""
#         word = choose_word()
#     letter1 = find_letter(word, 0)
#     letter2 = find_letter(word, 1)
#     letter3 = find_letter(word, 2)
#     letter4 = find_letter(word, 3)
#     letter5 = find_letter(word, 4)

#     context = {
#         "form": form,
#         "letter1": letter1,
#         "letter2": letter2,
#         "letter3": letter3,
#         "letter4": letter4,
#         "letter5": letter5,
#         "word": word,
#         "guess1": guess1,
#         "guess2": guess2,
#         "guess3": guess3,
#         "guess4": guess4,
#     }
    
#     return render(request, "hangman/gameplay.html", context)

# def gameplay(request):
#     if request.method == "POST":
#         form = request.POST
#         return redirect("play_hangman")
#     else:
#         form = ""

#     context = {
#         "form": form,
#     }

#     return render(request, "hangman/game.html", context)


