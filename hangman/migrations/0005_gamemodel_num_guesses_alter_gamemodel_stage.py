# Generated by Django 4.0.5 on 2022-06-30 20:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hangman', '0004_alter_gamemodel_word'),
    ]

    operations = [
        migrations.AddField(
            model_name='gamemodel',
            name='num_guesses',
            field=models.SmallIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='gamemodel',
            name='stage',
            field=models.SmallIntegerField(default=1),
        ),
    ]
