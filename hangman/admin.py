from django.contrib import admin

from hangman.models import GameModel

# Register your models here.
class GameModelAdmin(admin.ModelAdmin):
    pass

admin.site.register(GameModel, GameModelAdmin)